Spree.config do |config|  
  config.currency = 'EUR'
  config.currency_symbol_position = 'after'
  config.display_currency = false
  config.default_country_id = 188 # España
  config.prices_inc_tax = true
  config.shipment_inc_vat = true
end
